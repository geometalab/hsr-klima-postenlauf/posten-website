:title: Posten E
:slug: 441cd5fa44d8af49d55082f6d9a7a1506b751dd8
:nofooter:

[start=12]
. Ist das Heizen mit Holz CO2 positiv, negativ oder neutral? Warum?
.. Positiv, da durch das Verbrennen mehr CO2 entsteht als der Baum in seinem Leben aufnehmen konnte.
.. Positiv, da durch das Verbrennen CO2 entsteht und der lebende Baum kein CO2 aufnimmt.
.. Neutral, da durch das Verbrennen gleichviel CO2 entsteht wie der Baum in seinem Leben aufnehmen konnte.
.. Neutral, da Bäume kein CO2 aufnehmen und durch das Verbrennen kein CO2 entsteht.
.. Negativ, da durch das Verbrennen weniger CO2 entsteht als der Baum in seinem Leben aufnehmen konnte.
.. Negativ, da ein lebender Baum CO2 aufnimmt aber durch das Verbrennen kein CO2 entsteht.

////
Lösung:	Neutral, da durch das Verbrennen gleichviel CO2 entsteht wie der Baum in seinem Leben aufnehmen konnte.

Begründung: All das CO2, das der Baum in seinem Leben in sich speichert, wird bei seinem Tod wieder freigesetzt.
////

[start=13]
. Wie gross ist der CO2-Ausstoss pro Person in der Schweiz pro Jahr in Tonnen?
.. 2.8
.. 3.5
.. 5.4
.. 7.3

////
Lösung:	5.4 Tonnen CO2
////

[start=14]
. Ordne die aufgelisteten Transportmittel. Auf Platz 1 kommt dasjenige, dass die meisten Treibhausgase pro Passagier ausstösst, wenn sie dieselbe Strecke zurücklegen.
* Flugzeug
* Zug
* Auto
* Linienbus

////
Lösung: 1.	Flugzeug
        2.	Auto
        3.	Linienbus
        4.	Zug	
////