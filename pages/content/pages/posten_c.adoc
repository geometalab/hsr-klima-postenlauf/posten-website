:title: Posten C
:slug: e2be9a8314259f6f360f286be818209c6bbcb3c6
:nofooter:

[start=7]
. Welche Sparmassnahme verringert den täglichen Wasserverbrauch am meisten?
.. WC: Kleine statt grosse Spültaste benützen
.. Duschen statt baden
.. Wasserhahn während dem Zähneputzen abdrehen
.. Geschirrspüler verwenden statt von Hand spülen

////
Lösung: Duschen statt baden

Begründung: Wenn man duscht, statt zu baden, spart man dabei etwa 50 Liter Wasser.
////

[start=8]
. Schätzfrage: Wie viel Wasser braucht es um ein T-shirt herzustellen?

////
Lösung:	2495Liter
////

[start=9]
. Schätzfrage: Wie gross ist der CO2-Ausstoss der gesamten Schweiz pro Jahr in Tonnen?

////
Lösung: 46.4 Millionen Tonnen CO2
////

---

Info zu Frage 7: Das Einsparpotenzial aller Optionen als Summe ist am grössten. Somit ist es am besten wenn man alle umsetzen würde.